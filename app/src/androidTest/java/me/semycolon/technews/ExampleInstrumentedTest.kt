/*
 * Copyright (c) 2018.
 * Seyyed Hassan Asheghi
 * SemyColon.me
 * info@SemyColon.me
 *
 */

package me.semycolon.technews

import android.support.test.runner.AndroidJUnit4
import me.semycolon.technews.api.NetworkManager

import org.junit.Test
import org.junit.runner.RunWith

/**
 * Instrumented test, which will execute on an Android device.
 *
 * See [testing documentation](http://d.android.com/tools/testing).
 */
@RunWith(AndroidJUnit4::class)
class ExampleInstrumentedTest {
//    @Test
//    fun checkparcelizeworking(){
//        Post(12341234,"title","date","author","image ling","url")
//        val bundle = Bundle()
//
//    }



    @Test
    fun useAppContext() {
        // Context of the app under test.
        val net = NetworkManager.instance
        val api = net.digiatoService
        val call = api.getHome(1)
        val response = call.execute()
        if (response.isSuccessful){
            for (post in response.body()?.posts!!) {
                println("*****************************\n\n\n\n\n")
                println(post.title)
                println("*****************************\n\n\n\n\n")
            }
        }else{
            error(response.message().toString())
        }
    }



}
