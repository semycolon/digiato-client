/*
 * Copyright (c) 2018.
 * Seyyed Hassan Asheghi
 * SemyColon.me
 * info@SemyColon.me
 *
 */

package me.semycolon.technews

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.support.design.widget.Snackbar
import android.support.design.widget.NavigationView
import android.support.v4.app.Fragment
import android.support.v4.view.GravityCompat
import android.support.v7.app.ActionBarDrawerToggle
import android.support.v7.app.AppCompatActivity
import android.text.Editable
import android.text.TextWatcher
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.view.inputmethod.InputMethodManager
import android.widget.AutoCompleteTextView
import android.widget.TextView
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.app_bar_main.*
import me.semycolon.technews.extensions.logi
import me.semycolon.technews.mvp.screen.about_us.AboutUsActivity
import me.semycolon.technews.mvp.screen.category.CategoryFragment
import me.semycolon.technews.mvp.screen.category.TopicsFragment
import me.semycolon.technews.mvp.screen.category.topic.ShowTopicFragment
import me.semycolon.technews.mvp.screen.favorites.FavoriteFragment
import me.semycolon.technews.mvp.screen.home.HomeFragment
import me.semycolon.technews.mvp.screen.search.SearchFragment
import java.util.concurrent.TimeUnit
import android.support.v4.view.MenuItemCompat.getActionView
import android.app.SearchManager
import android.support.v7.widget.SearchView


class MainActivity : AppCompatActivity(), NavigationView.OnNavigationItemSelectedListener {
    enum class Which {
        HOME, FAV, CATEGORY, SEARCH
    }


    override fun onCreate(savedInstanceState: Bundle?) {
        if (isDark()) {
            setTheme(R.style.AppTheme_Dark)
        }

        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        window.decorView.layoutDirection = View.LAYOUT_DIRECTION_RTL
        setSupportActionBar(toolbar)
        fab.setOnClickListener { view ->
            Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                    .setAction("Action", null).show()
        }

        val toggle = ActionBarDrawerToggle(
                this, drawer_layout, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close)
        drawer_layout.addDrawerListener(toggle)
        toggle.syncState()

        nav_view.setNavigationItemSelectedListener(this)
        nav_view.setCheckedItem(R.id.nav_home)
        initNavHeader(nav_view.getHeaderView(0))
        loadFragment()

    }

    private fun isDark() = getSharedPreferences("main", Context.MODE_PRIVATE).getBoolean("isdark", false)

    private fun initNavHeader(view: View) {
        // view.setOnClickListener { Snackbar.make(findViewById(android.R.id.content),"Go to sign in",Snackbar.LENGTH_SHORT)


        //todo sign in stuff
        val signedIn = false
        if (signedIn) {
            //show user info
        } else {
            //
        }


        view.findViewById<TextView>(R.id.nav_head_subtitle).text = "زندگی با تکنولوژی"
        view.findViewById<TextView>(R.id.nav_head_title).text = "تک نیوز"
    }

    private fun onSearch(query: String) {
        val searchFragment = SearchFragment()
        searchFragment.make(this, query)
        loadFragment(searchFragment)
        if (drawer_layout.isDrawerOpen(GravityCompat.START)) {
            drawer_layout.closeDrawer(GravityCompat.START)
        }


    }

    override fun onResume() {
        super.onResume()
        checkCurrentNavItem()
    }

    override fun onBackPressed() {
        if (supportFragmentManager.backStackEntryCount == 1) {
            finish()
        }

        checkCurrentNavItem()

        if (drawer_layout.isDrawerOpen(GravityCompat.START)) {
            drawer_layout.closeDrawer(GravityCompat.START)
        } else {
            super.onBackPressed()
        }
    }

    private fun checkCurrentNavItem() {
        try {
            val f = supportFragmentManager.findFragmentById(R.id.main_content)
            when (f) {
                is HomeFragment -> {
                    currentWhich = Which.HOME
                    nav_view.setCheckedItem(R.id.nav_home)
                }
                is FavoriteFragment -> {
                    currentWhich = Which.FAV
                    nav_view.setCheckedItem(R.id.nav_fav)
                }
                is CategoryFragment -> {
                    currentWhich = Which.CATEGORY
                    nav_view.setCheckedItem(R.id.nav_category)
                }
                else -> {
                    currentWhich = Which.SEARCH
                    val size = nav_view.menu.size()
                    for (i in 0..size) {
                        nav_view.menu.getItem(i).isChecked = false
                    }
                }
            }

        } catch (e: Exception) {
            e.printStackTrace()
        }

    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        // Inflate the menu; this adds items to the action bar if it is present.
        menuInflater.inflate(R.menu.main, menu)
        if (isDark()) {
            menu.findItem(R.id.menu_main_change_theme).setIcon(R.drawable.ic_light_dark)
        }


        // Associate searchable configuration with the SearchView
        val searchManager = getSystemService(Context.SEARCH_SERVICE) as SearchManager
        val searchView = menu.findItem(R.id.action_search).actionView as SearchView
        searchView.setSearchableInfo(searchManager.getSearchableInfo(componentName))
        searchView.setOnQueryTextListener(object: SearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(query: String?): Boolean {
                if (query != null) {
                    onSearch(query)
                    searchView.setQuery(query,false)

                }

                return false
            }

            override fun onQueryTextChange(newText: String?) = false
        })

        return true
    }


    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify getAll parent activity in AndroidManifest.xml.
        return when (item.itemId) {
            R.id.menu_main_change_theme -> {

                getSharedPreferences("main", Context.MODE_PRIVATE).edit()
                        .putBoolean("isdark", !isDark()).apply()
                try {
                    HomeFragment.currentPage = 1
                    finish()
                    startActivity(intent)
                } catch (e: Exception) {
                    e.printStackTrace()
                }
                true
            }

            android.R.id.home -> {
                onBackPressed()
                return true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    private var currentWhich = Which.HOME

    override fun onNavigationItemSelected(item: MenuItem): Boolean {

        // Handle navigation view item clicks here.
        when (item.itemId) {

            R.id.nav_about -> {
                startActivity(Intent(this, AboutUsActivity::class.java))
            }
            R.id.nav_home -> {
                if (currentWhich != Which.HOME) {
                    currentWhich = Which.HOME
                    loadFragment()
                }
            }

            R.id.nav_fav -> {
                if (currentWhich != Which.FAV) {
                    currentWhich = Which.FAV
                    loadFragment()
                }
            }

            R.id.nav_category -> {
                if (currentWhich != Which.CATEGORY) {
                    currentWhich = Which.CATEGORY
                    loadFragment()
                }
            }
        }

        drawer_layout.closeDrawer(GravityCompat.START)
        return true
    }

    private fun loadFragment() {
        val ft = supportFragmentManager.beginTransaction()
        val fragment = getFragment()
        ft.replace(R.id.main_content, fragment)
        ft.addToBackStack(fragment?.tag)
        ft.commit()
    }

    private fun loadFragment(fragment: Fragment) {
        val ft = supportFragmentManager.beginTransaction()
        ft.replace(R.id.main_content, fragment)
        ft.addToBackStack(fragment.tag)
        ft.commit()
    }

    private fun getFragment(): Fragment? {
        return when (currentWhich) {
            Which.HOME -> {
                val homeFragment = HomeFragment()
                HomeFragment.currentPage = 1
                homeFragment.make(context = this)
                homeFragment
            }
            Which.FAV -> {
                val favFragment = FavoriteFragment()
                favFragment.make(context = this)
                favFragment
            }
            Which.CATEGORY -> {
                val fragment = CategoryFragment()
                fragment.make(this)
                fragment.setOnItemClickListener {
                    it.toString().logi()
                    //load show fragment
                    val topicsFragment = TopicsFragment()
                    topicsFragment.make(this)
                    topicsFragment.bindData(it.children)
                    topicsFragment.setOnItemClickListener(shit = {
                        val stf = ShowTopicFragment()
                        stf.make(this, it.id)
                        loadFragment(stf)
                        it.toString().logi()
                    })
                    loadFragment(topicsFragment)
                }
                fragment
            }

            else -> {
                null
            }
        }
    }
}
