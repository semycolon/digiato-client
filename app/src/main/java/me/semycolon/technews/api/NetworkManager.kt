/*
 * Copyright (c) 2018.
 * Seyyed Hassan Asheghi
 * SemyColon.me
 * info@SemyColon.me
 *
 */

package me.semycolon.technews.api

import retrofit2.Retrofit
import retrofit2.converter.moshi.MoshiConverterFactory
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor


class NetworkManager {
    private val retrofit: Retrofit
    val digiatoService: DigiatoService

    init {
        val interceptor = HttpLoggingInterceptor()
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY)
        val client = OkHttpClient.Builder().addInterceptor(interceptor).build()

        retrofit = Retrofit.Builder()
                .baseUrl("http://digiato.com")
                .addConverterFactory(MoshiConverterFactory.create())
                .client(client)
                .build()

        digiatoService = retrofit.create(DigiatoService::class.java)
    }

    companion object {
        val instance = NetworkManager()
    }
}
