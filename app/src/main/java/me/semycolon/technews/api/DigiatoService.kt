/*
 * Copyright (c) 2018.
 * Seyyed Hassan Asheghi
 * SemyColon.me
 * info@SemyColon.me
 *
 */

package me.semycolon.technews.api

import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Query
interface DigiatoService {
    @GET("app-feeder/")
    fun getHome(
            @Query("page_num") page_num: Int,
            @Query("action") action: String = "get_home",
            @Query("v") v: Int = 2
    ): Call<HomeResponse>

    @GET("app-feeder/")
    fun getPosts(
            @Query("page_num") page_num: Int,
            @Query("action") action: String = "get_posts",
            @Query("v") v: Int = 2
    ): Call<List<Post>>


    @GET("app-feeder/")
    fun getCats(
            @Query("action") action: String = "get_cats"
    ): Call<List<Category>>


    //"http://digiato.com/app-feeder/?action=get_posts&v=2&page_num=3&topic_id=30&posts_per_page=5
    @GET("app-feeder/")
    fun getTopic(
            @Query("topic_id") topic_id: Long,
            @Query("action") action: String = "get_posts",
            @Query("v") v: Int = 2,
            @Query("page_num") page_num: Int = 1,
            @Query("posts_per_page") posts_per_page: Int = 5
    ): Call<List<Post>>


    //app-feeder/?action=get_posts&v=2&search=ios&posts_per_page=5&page_num=2
    @GET("app-feeder/")
    fun search(@Query("action") action: String = "get_posts",
            @Query("page_num") page_num: Int = 1,
               @Query("search") search: String,
               @Query("posts_per_page") posts_per_page: Int = 5)
            : Call<List<Post>>



}

