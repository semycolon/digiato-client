/*
 * Copyright (c) 2018.
 * Seyyed Hassan Asheghi
 * SemyColon.me
 * info@SemyColon.me
 *
 */

package me.semycolon.technews.api

import android.annotation.SuppressLint
import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey
import android.os.Parcelable
import com.droidcba.kedditbysteps.commons.adapter.ViewType
import kotlinx.android.parcel.Parcelize
import me.semycolon.technews.mvp.screen.home.adapter.HomeAdapterConstants

@SuppressLint("ParcelCreator")
@Parcelize
data class HomeResponse(val posts: List<Post> = ArrayList(),
                        val ads: List<Ad> = ArrayList(),
                        val slider: List<Slide> = ArrayList()) : Parcelable


data class PostsResponse(
        val list: List<Post>)


@SuppressLint("ParcelCreator")
@Parcelize
@Entity(tableName = "favorites")
data class Post(@PrimaryKey var id: Long=-1,
                var title: String="",
                var date: String="",
                var author: String="",
                var image: String="",
                var url: String="",
                var short_url: String="",
                var content: String="",
                var topics: List<Topic> = ArrayList(),
                var sources: List<Source> = ArrayList()) : Parcelable,ViewType {
    override fun getViewType() = HomeAdapterConstants.TYPE.POST
}

@SuppressLint("ParcelCreator")
@Parcelize
data class Slide(val id: Long=-1,
                 val is_breaking_news: Int=-1,
                 val is_featured_post: Int=-1,
                 val title: String="",
                 val author: String="",
                 val image: String="",
                 val url: String="",
                 val short_url: String="",
                 val content: String="",
                 val comments_num: Int,
                 val topics: List<Topic> = ArrayList(),
                 val sources: List<Source> = ArrayList() ) : Parcelable , ViewType{
    override fun getViewType() = HomeAdapterConstants.TYPE.SLIDE
}

@SuppressLint("ParcelCreator")
@Parcelize
@Entity
data class Topic(var name: String="",
                 @PrimaryKey var id: Long=-1) : Parcelable

@SuppressLint("ParcelCreator")
@Parcelize
@Entity
data class Source(var ns_digi_post_source_name: String="",
                  @PrimaryKey var ns_digi_post_source_url: String="") : Parcelable
@SuppressLint("ParcelCreator")
@Parcelize
data class Ad(val image: String="",
              val url: String="",
              val title: String="") : Parcelable , ViewType{
    override fun getViewType() = HomeAdapterConstants.TYPE.AD

}

//category response
data class Category(val id :Int,
                    val name :String,
                    val children:List<Topic>)


data class CategoryResponse(val list:Category)

//....  http://digiato.com/app-feeder/?action=get_posts&v=2&page_num=3&topic_id=30&posts_per_page=5


