/*
 * Copyright (c) 2018.
 * Seyyed Hassan Asheghi
 * SemyColon.me
 * info@SemyColon.me
 *
 */

package me.semycolon.technews.pref

import android.content.Context
import android.content.SharedPreferences

class MyPreference(context: Context) {
    private val pref :SharedPreferences = context.getSharedPreferences("read",Context.MODE_PRIVATE)

    fun setRead(postId:Long,read:Boolean = true) = pref.edit().putBoolean("read_post_$postId",read).apply()

    fun isRead(postId: Long) = pref.getBoolean("read_post_$postId",false)


    fun getFontSize() = pref.getInt("font_size",20)

    fun setFontSize(int: Int) = pref.edit().putInt("font_size",int).apply()


}