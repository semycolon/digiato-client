/*
 * Copyright (c) 2018.
 * Seyyed Hassan Asheghi
 * SemyColon.me
 * info@SemyColon.me
 *
 */

package me.semycolon.technews.room;

import android.arch.persistence.room.TypeConverter;

import com.squareup.moshi.JsonAdapter;
import com.squareup.moshi.Moshi;
import com.squareup.moshi.Types;

import java.io.IOException;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import me.semycolon.technews.api.Source;
import me.semycolon.technews.api.Topic;

public class MyTypeConverter {
    private static JsonAdapter<List<Topic>> topicAdapter;
    static {
        Moshi moshi = new Moshi.Builder().build();
        Type type = Types.newParameterizedType(List.class, Topic.class);
        topicAdapter = moshi.adapter(type);
    }
    @TypeConverter
    public List<Topic> topiclistFromString(String json){
        try {
            List<Topic> topics = topicAdapter.fromJson(json);
            return topics;
        } catch (IOException e) {
            e.printStackTrace();
            return new ArrayList<Topic>();
        }
    }


    @TypeConverter
    public String topicListToString(List<Topic> list) {
        return topicAdapter.toJson(list);
    }

    private static JsonAdapter<List<Source>> sourceAdapter;
    static {
        Moshi moshi = new Moshi.Builder().build();
        Type type = Types.newParameterizedType(List.class, Source.class);
        sourceAdapter = moshi.adapter(type);
    }
    @TypeConverter
    public List<Source> sourcelistFromString(String json){
        try {
            List<Source> sources = sourceAdapter.fromJson(json);
            return sources;
        } catch (IOException e) {
            e.printStackTrace();
            return new ArrayList<Source>();
        }
    }


    @TypeConverter
    public String sourceListToString(List<Source> list) {
        return sourceAdapter.toJson(list);
    }
}


