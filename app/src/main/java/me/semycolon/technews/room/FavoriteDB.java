/*
 * Copyright (c) 2018.
 * Seyyed Hassan Asheghi
 * SemyColon.me
 * info@SemyColon.me
 *
 */

package me.semycolon.technews.room;

import android.arch.persistence.room.Database;
import android.arch.persistence.room.Room;
import android.arch.persistence.room.RoomDatabase;
import android.arch.persistence.room.TypeConverters;
import android.content.Context;

import me.semycolon.technews.api.Post;
import me.semycolon.technews.api.Source;
import me.semycolon.technews.api.Topic;

@Database(entities = {Topic.class,Source.class,Post.class}, version = 1,exportSchema = false)
@TypeConverters(MyTypeConverter.class)
public abstract class FavoriteDB extends RoomDatabase {
    public abstract FavoriteDao favoriteDao();

    private static FavoriteDB instance = null;
    public static FavoriteDB getInstance(Context context){
        if (instance == null){
            instance = Room.databaseBuilder(context,
                    FavoriteDB.class, "favorites_db").build();
        }
        return instance;
    }
}