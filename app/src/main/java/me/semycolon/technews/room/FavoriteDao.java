/*
 * Copyright (c) 2018.
 * Seyyed Hassan Asheghi
 * SemyColon.me
 * info@SemyColon.me
 *
 */

package me.semycolon.technews.room;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;

import java.util.List;

import me.semycolon.technews.api.Post;

@Dao
public interface FavoriteDao {
    @Query("select * from favorites")
    List<Post> fetchAll();

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    void insert(Post post);

    @Delete
    void delete(Post post);

    @Query("SELECT count(*) FROM favorites WHERE id = :id;")
    int count(long id);
}
