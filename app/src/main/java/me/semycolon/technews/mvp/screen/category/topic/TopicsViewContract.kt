/*
 * Copyright (c) 2018.
 * Seyyed Hassan Asheghi
 * SemyColon.me
 * info@SemyColon.me
 *
 */

package me.semycolon.technews.mvp.screen.category

import me.semycolon.technews.api.Topic
import me.semycolon.technews.mvp.common.ViewMvc

interface TopicsViewContract : ViewMvc {
    fun setTopicItemClickListener(listener:(topic:Topic) ->Unit)
    fun bindTopics(topics: List<Topic>)

}