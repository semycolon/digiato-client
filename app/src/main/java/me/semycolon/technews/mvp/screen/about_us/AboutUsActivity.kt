/*
 * Copyright (c) 2018.
 * Seyyed Hassan Asheghi
 * SemyColon.me
 * info@SemyColon.me
 *
 */

package me.semycolon.technews.mvp.screen.about_us

import android.content.Context
import android.content.Intent
import android.net.Uri
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import kotlinx.android.synthetic.main.activity_about_us.*
import me.semycolon.technews.R
import android.widget.Toast


class AboutUsActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        val isDark = getSharedPreferences("main", Context.MODE_PRIVATE).getBoolean("isdark", false)
        if (isDark) {
            setTheme(R.style.AppTheme_Dark)
        } else {
            setTheme(R.style.AppTheme_NoActionBar)
        }
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_about_us)
        about_developer_name.text= "سید حسن عاشقی"
        about_developer_name.setOnClickListener {
            try {
                val intent = Intent()
                intent.action = Intent.ACTION_VIEW
                intent.data = Uri.parse("http://SemyColon.me")
                startActivity(intent)
            } catch (e: Exception) {
                e.printStackTrace()
            }
        }
        about_developer_mail.text= "info@SemyColon.me"
        about_developer_mail.setOnClickListener {
            val emailIntent = Intent(Intent.ACTION_SEND)
            emailIntent.data = Uri.parse("mailto:")
            emailIntent.type = "text/plain"
            emailIntent.putExtra(Intent.EXTRA_EMAIL, "info@semycolon.me")
            emailIntent.putExtra(Intent.EXTRA_SUBJECT, "تک نیوز")
            //emailIntent.putExtra(Intent.EXTRA_TEXT, "Email message goes here")

            try {
                startActivity(Intent.createChooser(emailIntent, "Send mail..."))
                finish()
            } catch (ex: android.content.ActivityNotFoundException) {
                Toast.makeText(this, "There is no email client installed.", Toast.LENGTH_SHORT).show()
            }

        }


    }
}
