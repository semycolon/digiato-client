/*
 * Copyright (c) 2018.
 * Seyyed Hassan Asheghi
 * SemyColon.me
 * info@SemyColon.me
 *
 */

package me.semycolon.technews.mvp.screen.home.adapter

import android.support.v7.widget.RecyclerView
import android.view.ViewGroup
import android.widget.Toast
import com.droidcba.kedditbysteps.commons.adapter.ViewType
import com.droidcba.kedditbysteps.commons.adapter.ViewTypeDelegateAdapter
import kotlinx.android.synthetic.main.row_ad.view.*
import me.semycolon.technews.R
import me.semycolon.technews.api.Ad
import me.semycolon.technews.extensions.inflate
import me.semycolon.technews.extensions.loadGif

class AdAdapterDelegation :ViewTypeDelegateAdapter{
    override fun onCreateViewHolder(parent: ViewGroup): RecyclerView.ViewHolder  = ViewHolder(parent)

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, item: ViewType) {
        holder as ViewHolder
        holder.bind(item as Ad)
    }

    class ViewHolder(parent: ViewGroup) : RecyclerView.ViewHolder(parent.inflate(R.layout.row_ad)){
        private val img = itemView.row_ad_img

        fun bind(ad: Ad){
            img.loadGif(ad.image)
            img.setOnClickListener { Toast.makeText(img.context,ad.title,Toast.LENGTH_SHORT).show() }
        }
    }
}