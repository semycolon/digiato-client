/*
 * Copyright (c) 2018.
 * Seyyed Hassan Asheghi
 * SemyColon.me
 * info@SemyColon.me
 *
 */

package me.semycolon.technews.mvp.screen.category.topic

import android.content.Context
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.view.MenuItem
import android.view.View
import kotlinx.android.synthetic.main.activity_show_topic.*
import me.semycolon.technews.R
import me.semycolon.technews.extensions.logd

class ShowTopicActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        val isDark = getSharedPreferences("main", Context.MODE_PRIVATE).getBoolean("isdark", false)
        if (isDark) {
            setTheme(R.style.AppTheme_Dark)
        } else {
            setTheme(R.style.show_topic)
        }

        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_show_topic)
        setSupportActionBar(show_topic_toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setDisplayShowHomeEnabled(true)

        window.decorView.layoutDirection = View.LAYOUT_DIRECTION_RTL


        val name = intent.extras.getString("topic_name")
        title = name
        val id = intent.extras.getLong("topic_id")

        val ft = supportFragmentManager.beginTransaction()
        val topicFragment = ShowTopicFragment()
        topicFragment.make(this,id)
        ft.replace(R.id.activity_show_topic_content,topicFragment)
        ft.addToBackStack(topicFragment.tag)
        ft.commit()
    }


    override fun onBackPressed() {
        if(supportFragmentManager.backStackEntryCount == 1){
            supportFragmentManager.backStackEntryCount.toString().logd()
            finish()
        }
        super.onBackPressed()

    }


    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        if (item != null) {
            if (item.itemId == android.R.id.home){
                finish()
            }
        }
        return super.onOptionsItemSelected(item)
    }
}
