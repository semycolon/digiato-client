/*
 * Copyright (c) 2018.
 * Seyyed Hassan Asheghi
 * SemyColon.me
 * info@SemyColon.me
 *
 */

package me.semycolon.technews.mvp.screen.category

import android.content.Context
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.View
import android.view.ViewGroup
import android.view.animation.DecelerateInterpolator
import jp.wasabeef.recyclerview.animators.SlideInLeftAnimator
import kotlinx.android.synthetic.main.row_category.view.*
import me.semycolon.technews.R
import me.semycolon.technews.api.Topic
import me.semycolon.technews.extensions.inflate

class TopicsView(context: Context) : TopicsViewContract {
    private var rootView = context.inflate(R.layout.view_category)
    private var list:RecyclerView
    private var mAdapter:Adapter

    init {
        list  = rootView.findViewById(R.id.cat_recycler_view)
        mAdapter =Adapter()
    }
    override fun getRootView() = rootView

    private lateinit var listener: (topic: Topic) -> Unit

    override fun setTopicItemClickListener(listener: (topic: Topic) -> Unit) {
        this.listener = listener
    }

    override fun bindTopics(topicList: List<Topic>) {
        rootView.findViewById<View>(R.id.cat_progress_bar).visibility = View.GONE

        list.apply {
            adapter = mAdapter
            val layoutManager = LinearLayoutManager(context)
            setLayoutManager(layoutManager)
            itemAnimator = SlideInLeftAnimator(DecelerateInterpolator())
        }

        mAdapter.bind(topicList)

    }

    inner class Adapter : RecyclerView.Adapter<ViewHolder>() {

        private var dataList : List<Topic> = ArrayList<Topic>()
        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) = ViewHolder(parent)
        override fun getItemCount() = dataList.size


        override fun onBindViewHolder(holder: ViewHolder, position: Int) {
            holder as ViewHolder
            holder.bind(dataList[position])
        }

        fun bind(topicList: List<Topic>) {
            this.dataList = topicList
            notifyItemRangeInserted(0,topicList.size)
        }
    }
    inner class ViewHolder(parent: ViewGroup): RecyclerView.ViewHolder(parent.inflate(R.layout.row_category)) {
        private val view = itemView.row_cat_view
        private val text = itemView.row_cat_text

        fun bind(topic: Topic){
            text.text = topic.name
            view.setOnClickListener { listener(topic) }
        }

    }

}



