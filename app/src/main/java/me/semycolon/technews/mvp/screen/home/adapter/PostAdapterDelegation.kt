/*
 * Copyright (c) 2018.
 * Seyyed Hassan Asheghi
 * SemyColon.me
 * info@SemyColon.me
 *
 */

package me.semycolon.technews.mvp.screen.home.adapter

import android.content.Intent
import android.support.v7.widget.RecyclerView
import android.view.View
import android.view.ViewGroup
import com.droidcba.kedditbysteps.commons.adapter.ViewType
import com.droidcba.kedditbysteps.commons.adapter.ViewTypeDelegateAdapter
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import me.semycolon.technews.R
import me.semycolon.technews.extensions.inflate
import kotlinx.android.synthetic.main.row_post.view.*
import me.semycolon.technews.api.Post
import me.semycolon.technews.extensions.loadImage
import me.semycolon.technews.extensions.logd
import me.semycolon.technews.extensions.logi
import me.semycolon.technews.mvp.screen.show_post.ShowPostActivity
import me.semycolon.technews.pref.MyPreference
import me.semycolon.technews.room.FavoriteDB

class PostAdapterDelegation() : ViewTypeDelegateAdapter {


    override fun onCreateViewHolder(parent: ViewGroup): RecyclerView.ViewHolder = ViewHolder(parent)

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, item: ViewType) {
        holder as ViewHolder
        holder.bind(item as Post)
    }


    private var pref: MyPreference? = null

    inner class ViewHolder(parent: ViewGroup) : RecyclerView.ViewHolder(parent.inflate(R.layout.row_post)) {
        private val img = itemView.row_post_img
        private val title = itemView.row_post_title
        private val date = itemView.row_post_date
        private val author = itemView.row_post_author
        private val fav = itemView.row_post_fav
        private val card = itemView.row_post_card
        private val seen = itemView.row_post_seen


        fun bind(post: Post) {
            img.loadImage(post.image)
            title.text = post.title
            date.text = post.date
            author.text = post.author
            //todo is fav

            card.setOnClickListener {
                try {
                    val intent = Intent(img.context, ShowPostActivity::class.java)
                    intent.putExtra(ShowPostActivity.POST_TAG, post)
                    img.context.startActivity(intent)
                } catch (e: Exception) {
                    e.printStackTrace()
                }
                seen.visibility = View.VISIBLE
            }
            if (pref == null) {
                this@PostAdapterDelegation.pref = MyPreference(img.context)
            }
            if (pref!!.isRead(post.id)) {
                "seen post.id ${post.id}  isSeen : ${pref!!.isRead(post.id)}  post:$post".logd("debug")
                seen.visibility = View.VISIBLE
            }else{
                seen.visibility = View.GONE
            }

            initFav(post)
        }

        private fun initFav(post: Post) {
            Observable.just("hi")
                    .observeOn(Schedulers.io())
                    .map {
                        val dao = FavoriteDB.getInstance(img.context).favoriteDao()
                        val count = dao.count(post.id)
                        count > 0

                    }.observeOn(AndroidSchedulers.mainThread())
                    .map {
                        if (it) {
                            fav.setImageResource(R.drawable.ic_star_fill)
                            "1.is fav ${post.id} = true".logi()
                        } else {
                            fav.setImageResource(R.drawable.ic_star_empty)
                            "1.is fav  ${post.id} = false".logi()
                        }
                        it
                    }
                    .map{
                        "2. ${post.id} setting on click listener ... it = $it".logi()
                        if (!it) {
                            fav.setOnClickListener {
                                "onclick  ${post.id}: inserting post".logi()
                                fav.setImageResource(R.drawable.ic_star_fill)
                                Observable.just("insert")
                                        .observeOn(Schedulers.io())
                                        .map {
                                            val dao = FavoriteDB.getInstance(img.context).favoriteDao()
                                            dao.insert(post)
                                        }.observeOn(AndroidSchedulers.mainThread())
                                        .subscribe({
                                            initFav(post)
                                        })
                            }
                        } else {
                            fav.setOnClickListener{
                                "onclick  ${post.id}: deleting post".logi()
                                fav.setImageResource(R.drawable.ic_star_empty)
                                Observable.just("delete")
                                        .observeOn(Schedulers.io())
                                        .map {
                                            val dao = FavoriteDB.getInstance(img.context).favoriteDao()
                                            dao.delete(post)
                                        }
                                        .observeOn(AndroidSchedulers.mainThread())
                                        .subscribe({
                                            initFav(post)
                                        })
                            }

                        }
                    }
                    .subscribe()
        }
    }
}