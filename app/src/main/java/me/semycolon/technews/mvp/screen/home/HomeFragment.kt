/*
 * Copyright (c) 2018.
 * Seyyed Hassan Asheghi
 * SemyColon.me
 * info@SemyColon.me
 *
 */

package me.semycolon.technews.mvp.screen.home

import android.content.Context
import android.os.Bundle
import android.support.design.widget.Snackbar
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import me.semycolon.technews.api.NetworkManager

class HomeFragment : Fragment() {
    private lateinit var homeView: HomeView

    private lateinit var c: Context

    fun make(context: Context) {
        this.c = context
        this.homeView = HomeView(context)
        homeView.setListener(context, { loadMore() })

        homeView.swipeReload(true,{
            currentPage = 1
            Observable.just("let'sgo")
                    .observeOn(Schedulers.io())
                    .map {
                        val api = NetworkManager.instance.digiatoService
                        val call = api.getHome(HomeFragment.currentPage)
                        call.execute()

                    }.observeOn(AndroidSchedulers.mainThread())
                    .subscribe(
                            {
                                if (it.isSuccessful) {
                                    val result = it.body()
                                    if (result != null) {
                                        homeView.clearItems()
                                        if (currentPage == 1) {
                                            homeView.bindAd(result.ads)
                                            homeView.bindSlide(result.slider)
                                        }
                                        currentPage++
                                        homeView.bindPost(result.posts)
                                    } else {
                                        Snackbar.make(homeView.getRootView(), "Error : ${it.message()}", Snackbar.LENGTH_INDEFINITE)
                                                .setAction("RETRY", { loadMore() }).show()
                                    }
                                }

                                homeView.dissmissSwipeLoading()


                            },
                            {
                                Snackbar.make(homeView.getRootView(),"Error : ${it.message}",Snackbar.LENGTH_INDEFINITE)
                                        .setAction("RETRY",{loadMore()}).show()
                            })
        })
    }

    companion object {
        var currentPage = 1

    }
    private fun loadMore() {
        println("OnLoadMore () with currentPage$currentPage")
        if (currentPage ==1){
            Observable.just("let'sgo")
                    .observeOn(Schedulers.io())
                    .map {
                        val api = NetworkManager.instance.digiatoService
                        val call = api.getHome(HomeFragment.currentPage)
                        call.execute()

                    }.observeOn(AndroidSchedulers.mainThread())
                    .subscribe(
                            {
                                if (it.isSuccessful) {
                                    val result = it.body()
                                    if (result != null) {
                                        if (currentPage == 1) {
                                            homeView.bindAd(result.ads)
                                            homeView.bindSlide(result.slider)
                                        }
                                        currentPage++
                                        homeView.bindPost(result.posts)
                                    } else {
                                        Snackbar.make(homeView.getRootView(), "Error : ${it.message()}", Snackbar.LENGTH_INDEFINITE)
                                                .setAction("RETRY", { loadMore() }).show()
                                    }
                                }


                            },
                            {
                                Snackbar.make(homeView.getRootView(),"Error : ${it.message}",Snackbar.LENGTH_INDEFINITE)
                                        .setAction("RETRY",{loadMore()}).show()
                            })
        }else{
            Observable.just("let'sgo")
                    .observeOn(Schedulers.io())
                    .map {
                        val api = NetworkManager.instance.digiatoService
                        val call = api.getPosts(HomeFragment.currentPage)
                        call.execute()

                    }.observeOn(AndroidSchedulers.mainThread())
                    .subscribe(
                            {
                                if (it.isSuccessful) {
                                    val result = it.body()
                                    if (result != null) {
                                        currentPage++
                                        homeView.bindPost(result)
                                    } else {
                                        Snackbar.make(homeView.getRootView(), "Error : ${it.message()}", Snackbar.LENGTH_INDEFINITE)
                                                .setAction("RETRY", { loadMore() }).show()
                                    }
                                }


                            },
                            {
                                it.printStackTrace()
                                Snackbar.make(homeView.getRootView(),"Error : ${it.message}",Snackbar.LENGTH_INDEFINITE)
                                        .setAction("RETRY",{loadMore()}).show()
                            })
        }




    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?) = homeView.getRootView()

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        loadMore()
    }

    override fun onResume() {
        super.onResume()
        homeView.homeAdapter.notifyDataSetChanged()
    }
}