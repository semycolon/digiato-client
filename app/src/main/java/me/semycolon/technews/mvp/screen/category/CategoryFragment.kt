/*
 * Copyright (c) 2018.
 * Seyyed Hassan Asheghi
 * SemyColon.me
 * info@SemyColon.me
 *
 */

package me.semycolon.technews.mvp.screen.category

import android.content.Context
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import me.semycolon.technews.api.Category
import me.semycolon.technews.api.NetworkManager
import me.semycolon.technews.extensions.snack

class CategoryFragment :Fragment(){
    private lateinit var categoryView: CategoryView


    fun make(context: Context){
        categoryView = CategoryView(context)
    }

    fun setOnItemClickListener(shit :(cat :Category) ->Unit){
        categoryView.setCategoryItemClickListener(shit)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?)
            = categoryView.getRootView()


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        Observable.just("let's go")
                .observeOn(Schedulers.io())
                .map{
                    val call = NetworkManager.instance.digiatoService.getCats()
                    val response = call.execute()
                    if (response.isSuccessful){
                        if (response.body() != null) {
                            return@map response.body()
                        }else{
                            view.snack("ops response body is empty!")
                            return@map ArrayList<Category>()
                        }
                    }else{
                        return@map ArrayList<Category>()
                    }
                }
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    categoryView.bindCategories(it!!)
                }, {
                    it.printStackTrace()
                })
    }
}