/*
 * Copyright (c) 2018.
 * Seyyed Hassan Asheghi
 * SemyColon.me
 * info@SemyColon.me
 *
 */

package me.semycolon.technews.mvp.screen.home.adapter

import android.content.Intent
import android.support.v7.widget.RecyclerView
import android.view.ViewGroup
import com.droidcba.kedditbysteps.commons.adapter.ViewType
import com.droidcba.kedditbysteps.commons.adapter.ViewTypeDelegateAdapter
import kotlinx.android.synthetic.main.row_slide.view.*
import me.semycolon.technews.R
import me.semycolon.technews.api.Post
import me.semycolon.technews.api.Slide
import me.semycolon.technews.extensions.inflate
import me.semycolon.technews.extensions.loadImage
import me.semycolon.technews.mvp.screen.show_post.ShowPostActivity

class SlideAdapterDelegation : ViewTypeDelegateAdapter{
    override fun onCreateViewHolder(parent: ViewGroup): RecyclerView.ViewHolder = ViewHolder(parent)

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, item: ViewType) {
        holder as ViewHolder
        holder.bind(item as Slide)
    }


    class ViewHolder(parent: ViewGroup) : RecyclerView.ViewHolder(parent.inflate(R.layout.row_slide)){
        private val img = itemView.row_slide_img
        private val title = itemView.row_slide_title
        fun bind(slide: Slide){
            img.loadImage(slide.image)
            title.text = slide.title

            img.rootView.setOnClickListener{
                try {
                    val intent = Intent(img.context, ShowPostActivity::class.java)
                    val post = Post(title = slide.title, content = slide.content, author = slide.author, image = slide.image)
                    intent.putExtra(ShowPostActivity.POST_TAG,post)
                    img.context.startActivity(intent)
                } catch (e: Exception) {
                    e.printStackTrace()
                }

            }
        }
    }

}