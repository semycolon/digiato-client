/*
 * Copyright (c) 2018.
 * Seyyed Hassan Asheghi
 * SemyColon.me
 * info@SemyColon.me
 *
 */

package me.semycolon.technews.mvp.screen.favorites

import android.content.Context
import android.os.Bundle
import android.support.design.widget.Snackbar
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import me.semycolon.technews.extensions.logi
import me.semycolon.technews.mvp.screen.home.HomeView
import me.semycolon.technews.room.FavoriteDB


class FavoriteFragment :Fragment(){

    private lateinit var homeView: HomeView

    private lateinit var c: Context

    fun make(context: Context) {
        this.c = context
        this.homeView = HomeView(context)
        homeView.setListener(context,{})
    }


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?) = homeView.getRootView()

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        Observable.just("fuck it")
                .observeOn(Schedulers.io())
                .map{
                    val all = FavoriteDB.getInstance(context).favoriteDao().fetchAll()
                    "fetch all ${all.size}".logi()
                    all
                }
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    if (it.isNotEmpty()){
                        homeView.bindPostNoLoading(it)
                    }else{
                        Snackbar.make(homeView.getRootView(),"علاقه مندی ها خالی است !!!" , Snackbar.LENGTH_LONG).show()
                        homeView.homeAdapter.clearItems()
                    }
                })

    }
}