/*
 * Copyright (c) 2018.
 * Seyyed Hassan Asheghi
 * SemyColon.me
 * info@SemyColon.me
 *
 */

package me.semycolon.technews.mvp.screen.home.adapter

import android.support.v4.util.SparseArrayCompat
import android.support.v7.widget.RecyclerView
import android.view.ViewGroup
import com.droidcba.kedditbysteps.commons.adapter.ViewType
import com.droidcba.kedditbysteps.commons.adapter.ViewTypeDelegateAdapter
import me.semycolon.technews.api.Ad
import me.semycolon.technews.api.Post
import me.semycolon.technews.api.Slide
import java.util.*

class HomeAdapter : RecyclerView.Adapter<RecyclerView.ViewHolder>() {
    private var delegateAdapters = SparseArrayCompat<ViewTypeDelegateAdapter>()
    private var items: ArrayList<ViewType>
    private var loadingItem = object : ViewType {
        override fun getViewType() = HomeAdapterConstants.TYPE.LOADING
    }

    init {
        delegateAdapters.put(HomeAdapterConstants.TYPE.LOADING,LoadingDelegateAdapter())
        delegateAdapters.put(HomeAdapterConstants.TYPE.SLIDE, SlideAdapterDelegation())
        delegateAdapters.put(HomeAdapterConstants.TYPE.AD, AdAdapterDelegation())
        delegateAdapters.put(HomeAdapterConstants.TYPE.POST, PostAdapterDelegation())
        items = ArrayList()
        items.add(loadingItem)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) = delegateAdapters.get(viewType).onCreateViewHolder(parent)


    override fun getItemCount() = items.size

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) = delegateAdapters.get(getItemViewType(position)).onBindViewHolder(holder, items[position])


    override fun getItemViewType(position: Int) = items[position].getViewType()

    private fun getLastPosition() = if (items.lastIndex == -1) 0 else items.lastIndex

    fun addSlide(list: List<Slide>){
        items.addAll(0,list)
        notifyItemRangeInserted(0,list.size-1)
    }

    fun addAd(list: List<Ad>){
        items.addAll(0,list)
        notifyItemRangeInserted(0,list.size-1)
    }

    fun addPost(list: List<Post>){
        //remove the loading view
        val initPosintion = items.size -1
        items.removeAt(initPosintion)


        items.addAll(list)
        items.add(loadingItem)


       notifyItemRangeChanged(initPosintion,list.size+1 /*plus loading item*/ )
    }

    fun addPostNoLoading(list :List<Post>){
        items.clear()
        items.addAll(list)
        notifyItemRangeInserted(0,items.size -1 )
    }

    fun clearItems() {
        items.clear()
        notifyDataSetChanged()
    }
}
