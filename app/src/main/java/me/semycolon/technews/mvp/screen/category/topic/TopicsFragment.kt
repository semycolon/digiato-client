/*
 * Copyright (c) 2018.
 * Seyyed Hassan Asheghi
 * SemyColon.me
 * info@SemyColon.me
 *
 */

package me.semycolon.technews.mvp.screen.category

import android.content.Context
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.ViewGroup
import me.semycolon.technews.api.Topic

class TopicsFragment :Fragment(){
    private lateinit var topicsView: TopicsView


    fun make(context: Context){
        topicsView = TopicsView(context)
    }

    fun setOnItemClickListener(shit :(topic :Topic) ->Unit){
        topicsView.setTopicItemClickListener(shit)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?)
            = topicsView.getRootView()

    fun bindData(list:List<Topic>){
        topicsView.bindTopics(list)
    }
}