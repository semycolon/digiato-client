/*
 * Copyright (c) 2018.
 * Seyyed Hassan Asheghi
 * SemyColon.me
 * info@SemyColon.me
 *
 */

package me.semycolon.technews.mvp.screen.home

import android.content.Context
import android.support.v4.widget.SwipeRefreshLayout
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.View
import android.view.animation.DecelerateInterpolator
import jp.wasabeef.recyclerview.animators.SlideInLeftAnimator
import me.semycolon.technews.R
import me.semycolon.technews.api.Ad
import me.semycolon.technews.api.Post
import me.semycolon.technews.api.Slide
import me.semycolon.technews.extensions.InfiniteScrollListener
import me.semycolon.technews.extensions.inflate
import me.semycolon.technews.mvp.screen.home.adapter.HomeAdapter

class HomeView(context: Context) : HomeViewContract {
    override fun clearItems() {
        homeAdapter.clearItems()
    }

    override fun swipeReload(isActive: Boolean , onRefresh:()->Unit) {
        if (refreshDiv != null) {
            try {
                refreshDiv!!.isEnabled = isActive
                if (isActive){
                    refreshDiv!!.setOnRefreshListener {
                        onRefresh()
                    }
                }
            } catch (e: Exception) {
                e.printStackTrace()
            }
        }

    }

    val homeAdapter: HomeAdapter = HomeAdapter()
    private var rootView: View = context.inflate(R.layout.p_home)
    private val list: RecyclerView

    private var refreshDiv: SwipeRefreshLayout?

    init {
        list = rootView.findViewById(R.id.home_recycler_view)
        list.apply {
            adapter = homeAdapter
        }
        refreshDiv = rootView.findViewById<SwipeRefreshLayout>(R.id.home_swipre_refresh_div)
        swipeReload(false,{})

    }

    override fun getRootView(): View {
        return rootView
    }

    override fun setListener(context: Context, some_shit: () -> Unit) {
        val linearLayoutManager = LinearLayoutManager(context)
        //init recyclerViews tuff
        list.apply {
            layoutManager = linearLayoutManager
            clearOnScrollListeners()
            addOnScrollListener(InfiniteScrollListener(some_shit, linearLayoutManager))
         //   itemAnimator = SlideInLeftAnimator(DecelerateInterpolator())

        }
    }


    override fun bindPost(list: List<Post>) {
        homeAdapter.addPost(list)

    }

    override fun bindSlide(list: List<Slide>) {
        homeAdapter.addSlide(list)
    }

    override fun bindAd(list: List<Ad>) {
        homeAdapter.addAd(list)
    }

    fun bindPostNoLoading(it: List<Post>) {
        homeAdapter.addPostNoLoading(it)
    }



    fun dissmissSwipeLoading(){
        if (refreshDiv != null) {
            refreshDiv!!.isRefreshing = false
        }
    }
}
