/*
 * Copyright (c) 2018.
 * Seyyed Hassan Asheghi
 * SemyColon.me
 * info@SemyColon.me
 *
 */

package me.semycolon.technews.mvp.screen.home.adapter

class HomeAdapterConstants {
    internal object TYPE {
        val POST = 0
        val SLIDE = 1
        val AD = 2
        val LOADING = 3
    }
}
