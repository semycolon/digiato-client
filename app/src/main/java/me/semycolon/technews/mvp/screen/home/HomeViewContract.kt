/*
 * Copyright (c) 2018.
 * Seyyed Hassan Asheghi
 * SemyColon.me
 * info@SemyColon.me
 *
 */

package me.semycolon.technews.mvp.screen.home

import android.content.Context
import me.semycolon.technews.api.Ad
import me.semycolon.technews.api.Post
import me.semycolon.technews.api.Slide
import me.semycolon.technews.mvp.common.ViewMvc

interface HomeViewContract :ViewMvc {
    fun bindPost(list:List<Post>)
    fun bindSlide(list:List<Slide>)
    fun bindAd(list:List<Ad>)

    interface HomeViewListener{
        fun onPostClicked(post : Post)
        fun onAdClicked(ad : Ad)
        fun onSliderClicked(slide: Slide)
    }

    fun setListener(context: Context,some_shit:()->Unit)

    fun swipeReload(isActive :Boolean,action :()->Unit)

    fun clearItems()
}