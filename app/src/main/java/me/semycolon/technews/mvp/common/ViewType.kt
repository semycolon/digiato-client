/*
 * Copyright (c) 2018.
 * Seyyed Hassan Asheghi
 * SemyColon.me
 * info@SemyColon.me
 *
 */

package com.droidcba.kedditbysteps.commons.adapter

interface ViewType {
    fun getViewType(): Int
}