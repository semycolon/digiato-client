/*
 * Copyright (c) 2018.
 * Seyyed Hassan Asheghi
 * SemyColon.me
 * info@SemyColon.me
 *
 */

package me.semycolon.technews.mvp.screen.show_post

import android.content.Context
import android.content.Intent
import android.net.Uri
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.widget.TextView
import kotlinx.android.synthetic.main.fragment_show_post.*
import me.semycolon.technews.R
import me.semycolon.technews.api.Post
import me.semycolon.technews.extensions.OnSwipeTouchListener
import me.semycolon.technews.extensions.inflate
import me.semycolon.technews.extensions.loadImage
import me.semycolon.technews.mvp.screen.category.topic.ShowTopicActivity
import me.semycolon.technews.pref.MyPreference
import android.net.ConnectivityManager
import android.view.*
import android.webkit.WebSettings.LayoutAlgorithm
import android.webkit.WebSettings
import android.widget.CheckBox
import android.widget.SeekBar
import android.widget.Toast
import com.afollestad.materialdialogs.MaterialDialog
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import me.semycolon.technews.extensions.logi
import me.semycolon.technews.room.FavoriteDB


class ShowPostActivity : AppCompatActivity() {

    private var isdaaark = false;
    private lateinit var pref: MyPreference

    private lateinit var currentPost: Post
    override fun onCreate(savedInstanceState: Bundle?) {
        val isDark = getSharedPreferences("main", Context.MODE_PRIVATE).getBoolean("isdark", false)
        isdaaark = isDark
        if (isDark) {
            setTheme(R.style.AppTheme_Dark)
        } else {
            setTheme(R.style.AppTheme_NoActionBar)
        }

        super.onCreate(savedInstanceState)
        setContentView(R.layout.fragment_show_post)
        this.pref = MyPreference(this)


        window.decorView.layoutDirection = View.LAYOUT_DIRECTION_RTL
        //stuf

        //webview init
        val settings = show_p_webview.settings
        settings.javaScriptEnabled = false
        settings.defaultTextEncodingName = "utf-8"
        settings.layoutAlgorithm = LayoutAlgorithm.SINGLE_COLUMN
        settings.builtInZoomControls = false
        settings.defaultFontSize = (pref.getFontSize() / 1.5).toInt()
        val connectivityManager = getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        if (connectivityManager.activeNetworkInfo == null || !connectivityManager.activeNetworkInfo.isConnected) {
            settings.cacheMode = WebSettings.LOAD_CACHE_ELSE_NETWORK
        } else {
            settings.cacheMode = WebSettings.LOAD_DEFAULT
        }


        var post: Post? = null
        try {
            post = intent.getParcelableExtra<Post>(POST_TAG)
            currentPost = post
            bind(post)
        } catch (e: Exception) {
            e.printStackTrace()
            Toast.makeText(this,"مشکلی پیش آمده است" , Toast.LENGTH_SHORT).show()
            finish()
        }

        //sipe left listener
        show_p_body.setOnTouchListener(OnSwipeTouchListener(this))
        show_p_div_2.setOnTouchListener(OnSwipeTouchListener(this))
        show_p_webview.setOnTouchListener(OnSwipeTouchListener(this))

        setSupportActionBar(show_p_toolbar)
        show_p_toolbar.title = "تک نیوز"
        title = "تک نیوز"
        supportActionBar!!.title= "اخبار نکنولوژی"
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        supportActionBar!!.setDisplayShowHomeEnabled(true)
        show_p_toolbar_layout.setExpandedTitleTextAppearance(R.style.show_p_title)


       show_p_fav.setButtonDrawable(R.drawable.check_box_fav)

        if (post != null) initFav(post,show_p_fav)

    }

    private fun bind(post: Post) {
        show_p_date.text = post.date
        show_p_title.text = post.title
        show_p_writer.text = post.author
        val font = "@font-face {font-family: MyFont; src: url(\"file:///android_asset/fonts/sahel.ttf\");}"
        val string: String = if (isdaaark) {
            getString(R.string.content_style_dark)
        } else {
            getString(R.string.content_style)
        }


        pref.setRead(post.id,true)

        show_p_webview.loadDataWithBaseURL("", "<html><head><style>$string $font</style></head><body>${post.content}</body></html>", "text/html", "utf-8", "")
        show_p_img.loadImage(post.image)

        //add sources
        if (post.sources.isNotEmpty()) {
            show_p_source_title.visibility = View.VISIBLE
            post.sources.forEach {
                val view = applicationContext.inflate(R.layout.item_source)
                view.findViewById<TextView>(R.id.item_source_txt).text = it.ns_digi_post_source_name
                val url = it.ns_digi_post_source_url
                view.findViewById<View>(R.id.item_source_card).setOnClickListener {
                    val intent: Intent = Intent()
                    intent.action = Intent.ACTION_VIEW
                    intent.data = Uri.parse(url)
                    try{
                        startActivity(intent)
                    }catch (it:Throwable){
                        it.printStackTrace()
                        Toast.makeText(this,"مشکلی پیش آمده است",Toast.LENGTH_SHORT).show()
                    }

                }
                show_p_source_div.addView(view)
            }
        }

        if (post.topics.isNotEmpty()) {
            show_p_topic_title.visibility = View.VISIBLE
            post.topics.forEach {
                val view = applicationContext.inflate(R.layout.item_source)
                view.findViewById<TextView>(R.id.item_source_txt).text = it.name
                val name = it.name
                val id = it.id
                view.findViewById<View>(R.id.item_source_card).setOnClickListener {
                    val intent = Intent(applicationContext, ShowTopicActivity::class.java)
                    intent.putExtra("topic_id", id)
                    intent.putExtra("topic_name", name)
                    try {
                        startActivity(intent)
                    } catch (ex: android.content.ActivityNotFoundException) {
                        ex.printStackTrace()
                        Toast.makeText(this, "مشکلی پیش آمده است", Toast.LENGTH_SHORT).show()
                    }

                }
                show_p_topic_div.addView(view)
            }
        }
    }

    companion object {
        const val POST_TAG = "post_parcel"
    }


    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        MenuInflater(this).inflate(R.menu.show_post,menu)
        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        if (item != null) {
            if (item.itemId == android.R.id.home) {
                finish()
            }
            else if (item.itemId == R.id.menu_show_p_font_size){
                showDialogFontSize(this)
            }
        }
        return super.onOptionsItemSelected(item)
    }

    private fun showDialogFontSize(activity: ShowPostActivity) {
        val wrapInScrollView = true
        val view = inflate(R.layout.dialog_font_size)
        val seekbar = view.findViewById<SeekBar>(R.id.dialog_fontsize_seek)
        val text = view.findViewById<TextView>(R.id.dialog_fontsize_text)

        //set
        text.textSize = pref.getFontSize().toFloat()
        //set
        seekbar.progress = pref.getFontSize()

        var size = pref.getFontSize()
        seekbar.setOnSeekBarChangeListener(object: SeekBar.OnSeekBarChangeListener {
            override fun onProgressChanged(seekBar: SeekBar?, progress: Int, fromUser: Boolean) {
                size = progress
                text.textSize = size.toFloat()
                size.toString().logi()
                text.textSize.toString().logi(info = "textsize after")

            }

            override fun onStartTrackingTouch(seekBar: SeekBar?) {
            }

            override fun onStopTrackingTouch(seekBar: SeekBar?) {
            }
        })
        val dialog = MaterialDialog.Builder(this)
                .title("تغییر اندازه متن")
                .customView(view, wrapInScrollView)
                .positiveText("باشه")
                .negativeText("ولش")
                .dismissListener {
                    pref.setFontSize(text.textSize.toInt() / 3)
                    recreate()
                }.build()
        dialog.view.layoutDirection = View.LAYOUT_DIRECTION_RTL
        dialog.show()

    }

    private fun initFav(post: Post,fav: CheckBox) {
        Observable.just("hi")
                .observeOn(Schedulers.io())
                .map {
                    val dao = FavoriteDB.getInstance(this).favoriteDao()
                    val count = dao.count(post.id)
                    count > 0

                }.observeOn(AndroidSchedulers.mainThread())
                .map {
                    fav.isChecked = it
                    it
                }
                .map{
                    "2. ${post.id} setting on click listener ... it = $it".logi()
                    if (!it) {
                        fav.setOnClickListener {
                            "onclick  ${post.id}: inserting post".logi()
                            fav.isChecked = true
                            Observable.just("insert")
                                    .observeOn(Schedulers.io())
                                    .map {
                                        val dao = FavoriteDB.getInstance(this).favoriteDao()
                                        dao.insert(post)
                                    }.observeOn(AndroidSchedulers.mainThread())
                                    .subscribe({
                                        initFav(post,fav)
                                    })
                        }
                    } else {
                        fav.setOnClickListener{
                            "onclick  ${post.id}: deleting post".logi()
                            fav.isChecked = false
                            Observable.just("delete")
                                    .observeOn(Schedulers.io())
                                    .map {
                                        val dao = FavoriteDB.getInstance(this).favoriteDao()
                                        dao.delete(post)
                                    }
                                    .observeOn(AndroidSchedulers.mainThread())
                                    .subscribe({
                                        initFav(post,fav)
                                    })
                        }
                    }
                }
                .subscribe()
    }


}
