/*
 * Copyright (c) 2018.
 * Seyyed Hassan Asheghi
 * SemyColon.me
 * info@SemyColon.me
 *
 */

package me.semycolon.technews.mvp.screen.category

import android.content.Context
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.View
import android.view.ViewGroup
import android.view.animation.DecelerateInterpolator
import android.widget.RelativeLayout
import jp.wasabeef.recyclerview.animators.SlideInLeftAnimator
import kotlinx.android.synthetic.main.row_category.view.*
import me.semycolon.technews.R
import me.semycolon.technews.api.Category
import me.semycolon.technews.extensions.inflate

class CategoryView(context: Context) : CategoryViewContract {
    private var rootView = context.inflate(R.layout.view_category)
    private var list:RecyclerView
    private var mAdapter:Adapter

    init {
        list  = rootView.findViewById(R.id.cat_recycler_view)
        mAdapter =Adapter()
    }
    override fun getRootView() = rootView

    private lateinit var listener: (category: Category) -> Unit

    override fun setCategoryItemClickListener(listener: (category: Category) -> Unit) {
        this.listener = listener
    }

    override fun bindCategories(categoryList: List<Category>) {
        //hide progress bar
        rootView.findViewById<RelativeLayout>(R.id.cat_progress_bar).visibility = View.GONE

        list.apply {
            adapter = mAdapter
            val layoutManager = LinearLayoutManager(context)
            setLayoutManager(layoutManager)
            itemAnimator = SlideInLeftAnimator(DecelerateInterpolator())
        }

        mAdapter.bind(categoryList)

    }

    inner class Adapter : RecyclerView.Adapter<ViewHolder>() {


        private var dataList : List<Category> = ArrayList<Category>()
        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) = ViewHolder(parent)
        override fun getItemCount() = dataList.size


        override fun onBindViewHolder(holder: ViewHolder, position: Int) {
            holder as ViewHolder
            holder.bind(dataList[position])
        }

        fun bind(categoryList: List<Category>) {
            this.dataList = categoryList
            notifyItemRangeInserted(0,categoryList.size)
        }
    }
    inner class ViewHolder(parent: ViewGroup): RecyclerView.ViewHolder(parent.inflate(R.layout.row_category)) {
        private val view = itemView.row_cat_view
        private val text = itemView.row_cat_text

        fun bind(category: Category){
            text.text = category.name
            view.setOnClickListener { listener(category) }
        }

    }

}



