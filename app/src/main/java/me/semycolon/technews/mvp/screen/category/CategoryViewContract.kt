/*
 * Copyright (c) 2018.
 * Seyyed Hassan Asheghi
 * SemyColon.me
 * info@SemyColon.me
 *
 */

package me.semycolon.technews.mvp.screen.category

import me.semycolon.technews.api.Category
import me.semycolon.technews.mvp.common.ViewMvc

interface CategoryViewContract : ViewMvc {
    fun setCategoryItemClickListener(listener:(category:Category) ->Unit)
    fun bindCategories(categoryList: List<Category>)

}