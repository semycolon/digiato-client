/*
 * Copyright (c) 2018.
 * Seyyed Hassan Asheghi
 * SemyColon.me
 * info@SemyColon.me
 *
 */

package com.droidcba.kedditbysteps.commons.adapter

import android.support.v7.widget.RecyclerView
import android.view.ViewGroup

interface ViewTypeDelegateAdapter {

    fun onCreateViewHolder(parent: ViewGroup): RecyclerView.ViewHolder

    fun onBindViewHolder(holder: RecyclerView.ViewHolder, item: ViewType)
}