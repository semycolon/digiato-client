/*
 * Copyright (c) 2018.
 * Seyyed Hassan Asheghi
 * SemyColon.me
 * info@SemyColon.me
 *
 */

package me.semycolon.technews.mvp.screen.search

import android.content.Context
import android.os.Bundle
import android.support.design.widget.Snackbar
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import me.semycolon.technews.api.NetworkManager
import me.semycolon.technews.mvp.screen.home.HomeView


class SearchFragment : Fragment(){
    private lateinit var homeView: HomeView

    private lateinit var c: Context

    private lateinit var query: String
    var currentPage = 1

    fun make(context: Context, query:String) {
        this.query = query
        this.c = context
        this.homeView = HomeView(context)
        homeView.setListener(context, { loadMore() })

        homeView.swipeReload(true,{

            Observable.just("let'sgo")
                    .observeOn(Schedulers.io())
                    .map {
                        val res = NetworkManager.instance.digiatoService.search(search = query,page_num = currentPage).execute()
                        res

                    }.observeOn(AndroidSchedulers.mainThread())
                    .subscribe(
                            {
                                if (it.isSuccessful) {
                                    val result = it.body()
                                    if (result != null) {
                                        currentPage++
                                        homeView.bindPost(result)
                                    } else {
                                        Snackbar.make(homeView.getRootView(), "Error : ${it.message()}", Snackbar.LENGTH_INDEFINITE)
                                                .setAction("RETRY", { loadMore() }).show()
                                    }
                                }

                                homeView.dissmissSwipeLoading()


                            },
                            {
                                Snackbar.make(homeView.getRootView(),"Error : ${it.message}", Snackbar.LENGTH_INDEFINITE)
                                        .setAction("RETRY",{loadMore()}).show()
                            })
        })
    }

    private fun loadMore() {
        println("OnLoadMore () with currentPage$currentPage")
            Observable.just("let'sgo")
                    .observeOn(Schedulers.io())
                    .map {
                        NetworkManager.instance.digiatoService.search(search = query,page_num = currentPage).execute()
                    }.observeOn(AndroidSchedulers.mainThread())
                    .subscribe(
                            {
                                if (it.isSuccessful) {
                                    val result = it.body()
                                    if (result != null) {
                                        currentPage++
                                        homeView.bindPost(result)
                                    } else {
                                        Snackbar.make(homeView.getRootView(), "Error : ${it.message()}", Snackbar.LENGTH_INDEFINITE)
                                                .setAction("RETRY", { loadMore() }).show()
                                    }
                                }


                            },
                            {
                                Snackbar.make(homeView.getRootView(),"Error : ${it.message}", Snackbar.LENGTH_INDEFINITE)
                                        .setAction("RETRY",{loadMore()}).show()
                            })

    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?) = homeView.getRootView()

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        loadMore()
    }

    override fun onResume() {
        super.onResume()
        homeView.homeAdapter.notifyDataSetChanged()
    }
}