/*
 * Copyright (c) 2018.
 * Seyyed Hassan Asheghi
 * SemyColon.me
 * info@SemyColon.me
 *
 */

package me.semycolon.technews.mvp.screen.category.topic

import android.content.Context
import android.os.Bundle
import android.support.design.widget.Snackbar
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import me.semycolon.technews.api.NetworkManager
import me.semycolon.technews.mvp.screen.home.HomeView

class ShowTopicFragment : Fragment() {
    private lateinit var homeView: HomeView

    private var topicID: Long = -1


    fun make(context: Context, topic_id: Long) {
        homeView = HomeView(context)
        this.topicID = topic_id
        homeView.setListener(context,loadMore())
    }

    companion object {
        private var currentPage = 1
    }

    fun loadMore(): () -> Unit = {
        Observable.just("let's go again")
                .observeOn(Schedulers.io())
                .map {
                    val call = NetworkManager.instance.digiatoService.getTopic(topic_id = topicID, page_num = currentPage)
                    val response = call.execute()
                    if (response.isSuccessful && response.body() != null) {
                        currentPage++
                        return@map response.body()
                    } else {
                        throw Exception("مشکل در ارتباط با سرور") as Throwable
                    }

                }
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    homeView.bindPost(it!!)
                }, {
                    it.printStackTrace()
                    Snackbar.make(homeView.getRootView(),it.message.toString(),Snackbar.LENGTH_INDEFINITE).setAction("Retry",{ loadMore() })
                })
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?)=homeView.getRootView()

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        loadMore()()
    }
}