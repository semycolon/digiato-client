/*
 * Copyright (c) 2018.
 * Seyyed Hassan Asheghi
 * SemyColon.me
 * info@SemyColon.me
 *
 */

package me.semycolon.technews

import android.content.Intent
import me.semycolon.technews.api.HomeResponse
import me.semycolon.technews.api.NetworkManager
import me.semycolon.technews.extensions.logi
import org.junit.Test

import org.junit.Assert.*

/**
 * Example local unit test, which will execute on the development machine (host).
 *
 * See [testing documentation](http://d.android.com/tools/testing).
 */
class ExampleUnitTest {

    @Test
    fun search(){
        val res = NetworkManager.instance.digiatoService.search(search = "اندروید").execute()
        if (res.isSuccessful){
            res.body()!!.forEach {
                it.toString().logi()
                println()
                println()
                println()
            }
        }
    }
    @Test
    fun getCat(){
        val call = NetworkManager.instance.digiatoService.getCats()
        val response = call.execute()
        if (response.isSuccessful){
            print("success:))")
            if (response.body() != null) {
                response.body()!!.forEach {
                    println("=>>>$it.name")
                    it.children.forEach {
                        println("********>>>${it.name}")
                    }
                }
            }
        }else{
            error("ridi " + response.message())
        }

    }


    @Test
    fun parcelTest(){
        1
        val i = Intent()
        val homeResponse = HomeResponse(posts = ArrayList())
        i.putExtra("extra",homeResponse)
        val parcelableExtra = i.getParcelableExtra<HomeResponse>("extra")
        assertEquals(homeResponse,parcelableExtra)
    }



    @Test
    fun addition_isCorrect() {
        val net = NetworkManager.instance
        val api = net.digiatoService
        val call = api.getHome(1)
        val response = call.execute()
        if (response.isSuccessful){
            for (post in response.body()?.posts!!) {
                println("*****************************\n\n\n\n\n")
                println(post.title)
                println("*****************************\n\n\n\n\n")
            }
        }else{
            error(response.message().toString())
        }
    }
}
